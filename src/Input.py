#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Aug 11 15:35:27 2019

@author: antoine
"""
import sdl2.ext
import numpy

class Input():
    
    terminer = False
    touchesClavier = numpy.full((sdl2.SDL_NUM_SCANCODES), False)
    sourisX = 0
    sourisY = 0
    sourisRelX = 0
    sourisRelY = 0

    
    def UpdateEvents(self):
           events = sdl2.ext.get_events()
           self.sourisRelX,self.sourisRelY=0,0
           for event in events:
               if event.type == sdl2.SDL_QUIT or event.type == sdl2.SDL_APP_TERMINATING: self.terminer = True 
               elif event.type == sdl2.SDL_KEYDOWN : 
                   if event.key.keysym.scancode == sdl2.SDL_SCANCODE_ESCAPE : self.terminer = True 
                   else :self.touchesClavier[event.key.keysym.scancode] = True    
               elif event.type == sdl2.SDL_KEYUP: self.touchesClavier[event.key.keysym.scancode] = False
               elif event.type == sdl2.SDL_MOUSEMOTION: 
                   self.sourisX,self.sourisY,self.sourisRelX,self.sourisRelY = event.motion.x,event.motion.y,event.motion.xrel,event.motion.yrel
               elif event.type == sdl2.SDL_WINDOWEVENT_FOCUS_LOST :
                   sdl2.SDL_SetRelativeMouseMode(sdl2.SDL_DISABLE)

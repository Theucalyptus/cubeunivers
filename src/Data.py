#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Aug 12 10:46:31 2019

@author: antoine
"""

import json
import os
from os.path import isfile

class Fichier:
       
    data=dict()
    
    def Charger(self, path):   
        if os.path.isfile(path) == True:    
            self.path = path
            with open(path) as cfgFile:
                self.data = json.load(cfgFile) 
        else : print("Le fichier", path, "n'existe pas, utilisation des paramètres par défauts.")

    def GetVariable(self, nomVariable):
        return self.data[nomVariable]
            
    
    def SetVariable(self, nomVariable, variable):
        self.data[nomVariable] = variable
    
    def Sauvegarder(self):
        with open(self.path, 'w', encoding='utf-8') as cfgFile:
            json.dump(self.data, cfgFile, ensure_ascii=False, indent = 4)

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug 13 17:43:17 2019

@author: antoine
"""

import os
import glm
import random

import Terrain
import Data
import Camera
import Ciel


class Monde:
        
    data = Data.Fichier()
    renderDistance = 8
    playerCoordX, playerCoordY, playerCoordZ = 0,80,0
    lastUpdateTick = 0
    longueurJour = 120000
    seed = 0
    dayTime = 0
    
    def __init__(self, name, renderDistance, cameraSmoothness):
        self.name = name
        self.renderDistance = renderDistance
        self.cameraSmoothness = cameraSmoothness

        if os.path.isdir("Sauvegardes/Mondes/" + self.name) == False:
            self.Generer()
        else:   
            self.Charger()
 
    def Charger(self):
        self.data.Charger("Sauvegardes/Mondes/" + self.name + '/data.json')
        self.playerCoordX, self.playerCoordY, self.playerCoordZ, self.dayTime, self.seed = self.data.GetVariable('playerCoordX'), self.data.GetVariable('playerCoordY'), self.data.GetVariable('playerCoordZ'), self.data.GetVariable('DayTime'), self.data.GetVariable("seed")
        self.terrain = Terrain.Terrain(self.playerCoordX, self.playerCoordZ, self.seed, self.renderDistance,"Sauvegardes/Mondes/" + self.name + "/Chunks/")
        self.terrain.Charger()
        self.PostInit()
                
    def Generer(self):
        os.mkdir("Sauvegardes/Mondes/" + self.name)
        os.mkdir("Sauvegardes/Mondes/" + self.name + "/Chunks")
        os.mkdir("Sauvegardes/Mondes/" + self.name + "/Entities")
        self.seed = random.randint(0, 999999999)
        print(self.seed)
        self.terrain = Terrain.Terrain(self.playerCoordX, self.playerCoordZ, self.seed, self.renderDistance,"Sauvegardes/Mondes/" + self.name + "/Chunks/")
        self.terrain.Generer()
        self.data.path = "Sauvegardes/Mondes/" + self.name + '/data.json'
        self.PostInit()
        
        
        
    def PostInit(self):
        self.atmosphere = Ciel.Atmosphere(self.renderDistance, self.longueurJour)
        self.soleil = Ciel.Astres(self.renderDistance, self.longueurJour)
        self.camera = Camera.Camera(glm.vec3(self.playerCoordX, self.playerCoordY, self.playerCoordZ), glm.vec3(0,0,0), self.cameraSmoothness)

        
    def Sauvegarder(self):
        self.terrain.Sauvegarder()
        self.data.SetVariable('playerCoordX', self.playerCoordX)
        self.data.SetVariable('playerCoordZ', self.playerCoordZ)
        self.data.SetVariable('playerCoordY', self.playerCoordY)
        self.data.SetVariable('DayTime', self.dayTime)
        self.data.SetVariable('seed', self.seed)
        self.data.Sauvegarder()
    
    
    def Update(self, _input, sdlticks):
        delta = sdlticks - self.lastUpdateTick
        self.lastUpdateTick = sdlticks
        self.dayTime += delta
        if self.dayTime >= self.longueurJour : self.dayTime = 0
                
        self.camera.Update(_input)
        self.playerCoordX, self.playerCoordY, self.playerCoordZ = self.camera.position.x,self.camera.position.y,self.camera.position.z
                
        self.soleil.Update(self.dayTime)
        self.atmosphere.Update(self.dayTime, self.soleil.posSoleilY/self.soleil.r)
        self.terrain.Update(self.playerCoordX, self.playerCoordZ)
        
    
    
    
    def Rendre(self, projection):
        self.atmosphere.Rendre(projection, self.camera.getViewMatrice(), glm.translate(glm.mat4(1.0), glm.vec3(self.playerCoordX, self.playerCoordY, self.playerCoordZ)), glm.vec3(self.playerCoordX, self.playerCoordY, self.playerCoordZ))
        self.soleil.Rendre(projection, self.camera.getViewMatrice(), glm.translate(glm.mat4(1.0), glm.vec3(self.playerCoordX, self.playerCoordY, self.playerCoordZ)))
        self.terrain.Rendre(projection, self.camera.getViewMatrice(),  self.atmosphere.sunLightPower, self.atmosphere.sunLightColor,self.soleil.sunLightVecteur,glm.vec3(self.playerCoordX, self.playerCoordY, self.playerCoordZ), self.atmosphere.couleur)
        
        
        

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Aug 10 08:52:25 2019

@author: antoine
"""
from OpenGL.GL import *
import glm
import numpy

class Chunk :

    blocks = numpy.full((31,141,31), 0)

    def __init__(self, coord_x, coord_z, blocks=0, blocksCouleurs=0, verticesArray=0, couleursArray=0, normalesArray=0):
        self.coordX,self.coordZ=coord_x,coord_z
        self.blocks = blocks
        self.blocksCouleurs = blocksCouleurs
        self.buffer = glGenBuffers(1)
        self.verticesArray = verticesArray
        self.couleursArray = couleursArray
        self.normalesArray = normalesArray

        self.lenghtArrays = int(len(self.verticesArray)/3)



    def Update(self): #Met à jour le chunk - Fait la liste des faces de block à afficher
        verticesList = list() #Utilisation de liste pour raison de performance car numpy.append()  est TRES LENT
        couleursList = list()
        normalesList = list()
        for z in range(30):
            for x in range(30):
                for y in range(140):
                    if self.blocks[x,y,z] == 1:

                        if self.blocks[x+1,y,z] != 1 and y !=0: #Face Droite
                            verticesList.extend([x+1,y,z,x+1,y,z+1,x+1,y+1,z+1,x+1,y,z, x+1,y+1,z,x+1,y+1,z+1])
                            couleursList.extend(self.blocksCouleurs[x,y,z]+self.blocksCouleurs[x,y,z]+self.blocksCouleurs[x,y,z]+self.blocksCouleurs[x,y,z]+self.blocksCouleurs[x,y,z]+self.blocksCouleurs[x,y,z])
                            normalesList.extend([1.0,0.0,0.0,1.0,0.0,0.0,1.0,0.0,0.0,1.0,0.0,0.0,1.0,0.0,0.0,1.0,0.0,0.0])

                        if self.blocks[x-1,y,z] !=1 and y !=0: #Face Gauche
                            verticesList.extend([x,y,z,x,y,z+1,x,y+1,z+1,x,y,z,x,y+1,z,x,y+1,z+1])
                            couleursList.extend(self.blocksCouleurs[x,y,z]+self.blocksCouleurs[x,y,z]+self.blocksCouleurs[x,y,z]+self.blocksCouleurs[x,y,z]+self.blocksCouleurs[x,y,z]+self.blocksCouleurs[x,y,z])
                            normalesList.extend([-1.0,0.0,0.0,-1.0,0.0,0.0,-1.0,0.0,0.0,-1.0,0.0,0.0,-1.0,0.0,0.0,-1.0,0.0,0.0])


                        if self.blocks[x,y,z+1] !=1 and y !=0:
                            verticesList.extend([x,y,z+1,x+1,y,z+1,x+1,y+1,z+1,x,y,z+1,x,y+1,z+1,x+1,y+1,z+1])
                            couleursList.extend(self.blocksCouleurs[x,y,z]+self.blocksCouleurs[x,y,z]+self.blocksCouleurs[x,y,z]+self.blocksCouleurs[x,y,z]+self.blocksCouleurs[x,y,z]+self.blocksCouleurs[x,y,z])
                            normalesList.extend([0.0,0.0,1.0,0.0,0.0,1.0,0.0,0.0,1.0,0.0,0.0,1.0,0.0,0.0,1.0,0.0,0.0,1.0])


                        if self.blocks[x,y,z-1] !=1 and y !=0:
                            verticesList.extend([x,y,z,x+1,y,z,x+1,y+1,z,x,y,z,x,y+1,z,x+1,y+1,z])
                            couleursList.extend(self.blocksCouleurs[x,y,z]+self.blocksCouleurs[x,y,z]+self.blocksCouleurs[x,y,z]+self.blocksCouleurs[x,y,z]+self.blocksCouleurs[x,y,z]+self.blocksCouleurs[x,y,z])
                            normalesList.extend([0.0,0.0,-1.0,0.0,0.0,-1.0,0.0,0.0,-1.0,0.0,0.0,-1.0,0.0,0.0,-1.0,0.0,0.0,-1.0])


                        if self.blocks[x,y-1,z] !=1 and y !=0: #Face Dessous
                           verticesList.extend([x,y,z,x+1,y,z,x+1,y,z+1,x,y,z,x,y,z+1,x+1,y,z+1])
                           couleursList.extend(self.blocksCouleurs[x,y,z]+self.blocksCouleurs[x,y,z]+self.blocksCouleurs[x,y,z]+self.blocksCouleurs[x,y,z]+self.blocksCouleurs[x,y,z]+self.blocksCouleurs[x,y,z])
                           normalesList.extend([0.0,-1.0,0.0,0.0,-1.0,0.0,0.0,-1.0,0.0,0.0,-1.0,0.0,0.0,-1.0,0.0,0.0,-1.0,0.0])

                        if self.blocks[x,y+1,z] !=1: #Face Dessus
                            verticesList.extend([x,y+1,z,x+1,y+1,z,x+1,y+1,z+1,x,y+1,z,x,y+1,z+1,x+1,y+1,z+1])
                            couleursList.extend(self.blocksCouleurs[x,y,z]+self.blocksCouleurs[x,y,z]+self.blocksCouleurs[x,y,z]+self.blocksCouleurs[x,y,z]+self.blocksCouleurs[x,y,z]+self.blocksCouleurs[x,y,z])
                            normalesList.extend([0.0,1.0,0.0,0.0,1.0,0.0,0.0,1.0,0.0,0.0,1.0,0.0,0.0,1.0,0.0,0.0,1.0,0.0])

        self.verticesArray = numpy.asarray(verticesList, dtype=numpy.float32)
        self.couleursArray = numpy.asarray(couleursList, dtype=numpy.float32)
        self.normalesArray =  numpy.asarray(normalesList, dtype=numpy.float32)

        self.lenghtArrays = int(len(self.verticesArray)/3)


    def GenVAOVBO(self):

        glBindBuffer(GL_ARRAY_BUFFER, self.buffer) #Mise a jour du contenu du VBO
        glBufferData(GL_ARRAY_BUFFER, self.verticesArray.nbytes + self.couleursArray.nbytes + self.verticesArray.nbytes, None, GL_STATIC_DRAW)
        glBufferSubData(GL_ARRAY_BUFFER, 0, self.verticesArray.nbytes, self.verticesArray)
        glBufferSubData(GL_ARRAY_BUFFER, self.verticesArray.nbytes, self.couleursArray.nbytes, self.couleursArray)
        glBufferSubData(GL_ARRAY_BUFFER, self.verticesArray.nbytes + self.couleursArray.nbytes, self.normalesArray.nbytes, self.normalesArray)
        glBindBuffer(GL_ARRAY_BUFFER, 0)

        self.array = glGenVertexArrays(1) # Generation et configuration du VAO (contient les calls opengl d'enregistrement du VBO avant rendu)
        glBindVertexArray(self.array)
        glBindBuffer(GL_ARRAY_BUFFER, self.buffer)
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE,0, ctypes.c_void_p(0))
        glEnableVertexAttribArray(0)
        glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE,0, ctypes.c_void_p(self.verticesArray.nbytes))
        glEnableVertexAttribArray(1)
        glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE,0, ctypes.c_void_p(self.verticesArray.nbytes + self.couleursArray.nbytes))
        glEnableVertexAttribArray(2)
        glBindBuffer(GL_ARRAY_BUFFER, 0)
        glBindVertexArray(0)


    def Rendre(self, shader, projection, view, sunLightPower, sunLightColor, sunLightVecteur, playerPos, skyColor):

        model = glm.mat4(1.0)
        model = glm.translate(model,glm.vec3(self.coordX*30,0.0,self.coordZ*30))

        shader.Utiliser(True)


        glBindVertexArray(self.array)
        shader.UniformMat4('projection', projection)
        shader.UniformMat4('model', model)
        shader.UniformMat4('view', view)
        shader.UniformFloat('sunLightPuissance', min(sunLightPower, 1.0))
        shader.UniformVec3('sunLightColor', sunLightColor)
        shader.UniformVec3('sunLightVecteur', sunLightVecteur)
        shader.UniformVec3('playerPos', playerPos)
        shader.UniformVec3('skyColor', skyColor)

        glDrawArrays(GL_TRIANGLES, 0, self.lenghtArrays)
        glBindVertexArray(0)
        shader.Utiliser(False)




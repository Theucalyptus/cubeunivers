#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug 13 18:46:55 2019

@author: antoine
"""

import glm
import numpy
import ctypes
from OpenGL.GL import *
from OpenGL.GL.shaders import *

import Shader

class Atmosphere:
    
    def __init__(self, renderDistance, dureeJour):
        size = (renderDistance+4)/2*32
        verticesList = [-size, -40, size, size, -40, size, size, size, 0, -size, -40, size, -size,size,0,size,size,0,
                        -size, -40, -size, size, -40, -size, size, size, 0, -size, -40, -size, -size,size,0,size,size,0,
                        size, -40, size,size, size, 0,size, -40, -size, -size, -40, size,-size, size, 0,-size, -40, -size
                        ]
        self.longueurJour = dureeJour
        
        self.vertices = numpy.asarray(verticesList, dtype=numpy.float32)
        self.SetVBOVAO()
        self.shader = Shader.Shader("ciel")
        self.couleur = glm.vec3(1.0,1.0,1.0)
        self.ambLightPower = 0
        self.ambLightColor = glm.vec3(1.0,1.0,1.0)

    
    def SetVBOVAO(self):
        self.vbo = glGenBuffers(1) #Création du VBO
        glBindBuffer(GL_ARRAY_BUFFER, self.vbo)
        glBufferData(GL_ARRAY_BUFFER, self.vertices.nbytes, self.vertices, GL_STATIC_DRAW)
        glBindBuffer(GL_ARRAY_BUFFER, 0)
        
        self.vao = glGenVertexArrays(1) #Création du VAO
        glBindVertexArray(self.vao)
        glBindBuffer(GL_ARRAY_BUFFER, self.vbo)
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, ctypes.c_void_p(0))
        glEnableVertexAttribArray(0)
        glBindBuffer(GL_ARRAY_BUFFER, 0)
        glBindVertexArray(0)

    
    
    def Rendre(self, projection, view, model, playerPos):
        
        self.shader.Utiliser(True)
        glBindVertexArray(self.vao)
        self.shader.UniformMat4("projection", projection)
        self.shader.UniformMat4("view", view)
        self.shader.UniformMat4("model", model)
        self.shader.UniformVec3("couleur", self.couleur)
        self.shader.UniformFloat("sunLightPuissance", self.sunLightPower)
        self.shader.UniformVec3("playerPos", playerPos)
        glDrawArrays(GL_TRIANGLES, 0, int(len(self.vertices)/3))        
        glBindVertexArray(0)
        self.shader.Utiliser(False)

        
        
    def Update(self, dayTime, sunPosYRatio):        

        self.coefNuit = max(-sunPosYRatio, 0)
        self.coefNuit = 4*self.coefNuit
        self.coefNuit = min(self.coefNuit, 1.0)

        self.coefOrange = 1-sunPosYRatio*1.5
        self.coefOrange = min(self.coefOrange, 1.0)
        self.coefOrange -= self.coefNuit
        self.coefOrange = max(self.coefOrange, 0.0)
        
        self.sunLightPower = 1-self.coefNuit - self.coefOrange/2
        self.sunLightColor = glm.vec3(1.0,1.0,1.0) * 1.5-self.coefOrange + glm.vec3(1.0,0.54,0.4) * self.coefOrange/2
        
            
        self.couleur = (glm.vec3(0.0,0.5,1.0) + glm.vec3(1.0,0.5,0.0)*(self.coefOrange+self.coefNuit))  - glm.vec3(0.0,0.46,0.6)*self.coefOrange - glm.vec3(1.0,1.0,0.95)*self.coefNuit


import math
class Astres:
    
    def __init__(self, renderDistance, dureeJour):
        self.r = (renderDistance+3)/2*30
        verticesList = [-0.5,0,-0.5,       0.5,0,-0.5,         0.5,0,0.5,     -0.5,0,-0.5,         -0.5,0,0.5,  0.5,0,0.5]
        
        self.verticesCarré = numpy.asarray(verticesList, dtype=numpy.float32)
        self.SetVBOVAO()
        self.shader = Shader.Shader("astres")
        self.couleurSoleil = glm.vec3(1.0,1.0,0.0)
        self.couleurLune = glm.vec3(0.8,0.8,0.8)

        self.longueurJour = dureeJour
        
        self.posSoleilX, self.posSoleilY, self.angle = 0, self.r, 90.0

    
    def SetVBOVAO(self):
        self.vbo = glGenBuffers(1) #Création du VBO
        glBindBuffer(GL_ARRAY_BUFFER, self.vbo)
        glBufferData(GL_ARRAY_BUFFER, self.verticesCarré.nbytes, self.verticesCarré, GL_STATIC_DRAW)
        glBindBuffer(GL_ARRAY_BUFFER, 0)
        
        self.vao = glGenVertexArrays(1) #Création du VAO
        glBindVertexArray(self.vao)
        glBindBuffer(GL_ARRAY_BUFFER, self.vbo)
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, ctypes.c_void_p(0))
        glEnableVertexAttribArray(0)
        glBindVertexArray(0) 
    
    
    def Update(self, dayTime):
        if dayTime <= self.longueurJour/2:
            sunX = dayTime/(self.longueurJour/2)
        else : sunX = 1-(dayTime-self.longueurJour/2)/(self.longueurJour/2)
          
        sunX = (-1+sunX*2)*self.r        
        u = (abs(sunX))**2
        sunYCarre = (self.r**2)-u
        sunY = math.sqrt(sunYCarre)
        if dayTime >= self.longueurJour/2:
             sunY = -sunY
    
        angle = math.asin(abs(sunX)/self.r)
        
        if sunX >=0: angle = -angle
        
        self.posSoleilX, self.posSoleilY, self.angle = sunX, sunY, angle
        
        self.sunLightVecteur = glm.vec3(self.posSoleilX/self.r, self.posSoleilY/self.r, 0.0)
    
    def Rendre(self, projection,view, model):
        
        model1 = glm.translate(model, glm.vec3(self.posSoleilX,self.posSoleilY,0.0))
        model1 = glm.scale(model1, glm.vec3(10,10,10))
        model1 = glm.rotate(model1, self.angle, glm.vec3(0.0,0.0,1.0))
    
        self.shader.Utiliser(True)
        glBindVertexArray(self.vao)
        self.shader.UniformMat4("projection", projection)
        self.shader.UniformMat4("view", view)
        self.shader.UniformMat4("model", model1)
        self.shader.UniformVec3("couleur", self.couleurSoleil)
        glDrawArrays(GL_TRIANGLES, 0, int(len(self.verticesCarré)/3))        

    
        model1 = glm.translate(model, glm.vec3(-self.posSoleilX,-self.posSoleilY,0.0))
        model1 = glm.scale(model1, glm.vec3(4,4,4))
        model1 = glm.rotate(model1, self.angle, glm.vec3(0.0,0.0,-1.0))
        
        
        self.shader.UniformMat4("model", model1)
        self.shader.UniformVec3("couleur", self.couleurLune)
        glDrawArrays(GL_TRIANGLES, 0, int(len(self.verticesCarré)/3))
        glBindVertexArray(0) 
        self.shader.Utiliser(False)

        
        
        
        
        
        
        
        
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Aug 17 20:02:20 2019

@author: antoine
"""

from opensimplex import OpenSimplex
import numpy

from Chunk import Chunk


def GenChunk(coordChunkX, coordChunkY, seed):        
    biomeID = 0  
    
    if biomeID == 0:
        chunk = GenPlaine(seed, coordChunkX, coordChunkY)  
    return chunk


def SauvegarderChunk(chunk, path):
    numpy.savez_compressed(path+"x" + str(chunk.coordX) + "_z" + str(chunk.coordZ) + ".npz", blocks=chunk.blocks, blocksCouleurs=chunk.blocksCouleurs, verticesArray=chunk.verticesArray, couleursArray=chunk.couleursArray, normalesArray=chunk.normalesArray)
    
def ChargerChunk(coordX, coordZ, path):
    data = numpy.load(path+"x" + str(int(coordX)) + "_z" + str(int(coordZ)) + ".npz", allow_pickle=True)
    blocks = data['blocks']
    blocksCouleurs = data['blocksCouleurs']
    verticesArray = data['verticesArray']
    couleursArray = data['couleursArray']
    normalesArray = data['normalesArray']
        
    chunk = Chunk(int(coordX), int(coordZ), blocks, blocksCouleurs, verticesArray, couleursArray, normalesArray)
    chunk.GenVAOVBO()
    return chunk

def GenPlaine(seed, coordChunkX, coordChunkY):
    bruit = OpenSimplex(seed)
    
    couleurBaseSurface = [0.0,0.80,0.0]
    couleurBaseTerre = [1.0,0.67,0.20, 1.0]
    
    variationCouleur = numpy.empty((30,30))
    decorations = numpy.empty((30,30))
    reliefMap = numpy.empty((30,30), dtype=numpy.uint8)
    
    for x in range(30):
        for y in range(30):
            variationCouleur[x,y] = bruit.noise2d((coordChunkX*30+x)/20,(coordChunkY*30+y)/20)/2
            reliefMap[x,y] = int(40+bruit.noise2d((coordChunkX*30+x)/60,(coordChunkY*30+y)/60)*5)
            print(reliefMap[x,y])
            decorations[x,y] = bruit.noise2d((coordChunkX*30+x),(coordChunkY*30+y))
                
    blocks = numpy.full((35,141,35), 0)
    couleurs = numpy.empty((35,141,35), dtype=numpy.object)
    
    for x in range(30):
        for z in range(30):  
            for y in range(reliefMap[x,z]):
                if blocks[x,y,z] == 0:                      
                    blocks[x,y,z] = 1  
                    if y+1 == reliefMap[x,z]:
                        couleur = [ min(couleurBaseSurface[0], 1.0),min(couleurBaseSurface[1], 1.0),min(couleurBaseSurface[2] + variationCouleur[x,z], 1.0), 1.0]
                        couleurs[x,y,z] = couleur
                    else: couleurs[x,y,z] = couleurBaseTerre
            
            if decorations[x,z] >= 0.86:
                for i in range(8):
                    blocks[x,y+i+1,z] = 1
                    couleurs[x,y+i+1,z] = [0.30,0.15,0.0,1.0]
                for u in range(3):
                    for v in range(3):
                        for n in range(3):
                            if blocks[x+u-1, reliefMap[x,z]+n+8, v+z-1] == 0:  
                                blocks[x+u-1, reliefMap[x,z]+n+8, v+z-1] = 1
                                couleurs[x+u-1, reliefMap[x,z]+n+8, v+z-1] = [0.85,0.1,1.0,1.0]

            
            elif decorations[x,z] <= -0.86:   
                for u in range(3):
                    for v in range(3):
                        for n in range(3):
                            if blocks[x+u, reliefMap[x,z]+n+1, v+z] == 0:  
                                blocks[x+u, reliefMap[x,z]+n+1, v+z] = 1
                                couleurs[x+u, reliefMap[x,z]+n+1, v+z] = [0.8,0.8,0.8,1.0]

                    

    chunk = Chunk(int(coordChunkX), int(coordChunkY), blocks, couleurs)
    chunk.Update()
    chunk.GenVAOVBO()
    return chunk
    
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Aug 11 17:23:34 2019

@author: antoine
"""
import glm
import sdl2.ext

class Camera:
        
    position = glm.vec3(0.0,0.0,0.0)
    orientation = glm.vec3(0.0,0.0,0.0)
    cible = glm.vec3(0.0,0.0,0.0)
    directionHaut = glm.vec3(0.0,1.0,0.0)
    deltaXRel, deltaYRel = 0,0

    phi = 0
    theta = 0
    
    def __init__(self, position, orientation, smoothness=20):
        self.position,self.orientation=position,orientation
        self.smoothness = smoothness
        
    def Deplacer(self, devant=False, derriere=False, droite=False, gauche=False):
        vecteurAxeX = glm.normalize(glm.cross(self.directionHaut, self.orientation))
        
        if devant == True :  self.position = self.position + self.orientation * 0.2
        if derriere == True : self.position = self.position - self.orientation * 0.2
        if droite == True :self.position = self.position - vecteurAxeX * 0.2
        if gauche == True :self.position = self.position + vecteurAxeX * 0.2        
        self.Cibler()
            
    def Orienter(self, xRel, yRel):
        
        self.deltaXRel += xRel
        self.deltaYRel += yRel
        
        if self.phi-(self.deltaYRel/self.smoothness)/2 <= -90 : 
            self.phi = -89.9
            self.deltaYRel = 0
        elif self.phi-(self.deltaYRel/self.smoothness)/2 >= 90: 
            self.phi = 89.9
            self.deltaYRel = 0
        else : self.phi -= (self.deltaYRel/self.smoothness)/2 
        
        self.theta += (self.deltaXRel/self.smoothness)/2 
        
        if self.theta < -360 : self.theta += 360
        elif self.theta > 360 : self.theta -= 360
        
        self.orientation = glm.normalize(glm.vec3(  
                glm.cos(glm.radians(self.phi))*glm.cos(glm.radians(self.theta)), 
                glm.sin(glm.radians(self.phi)), 
                glm.cos(glm.radians(self.phi))*glm.sin(glm.radians(self.theta))))
        self.Cibler()
        
        self.deltaXRel -= self.deltaXRel/self.smoothness
        self.deltaYRel -= self.deltaYRel/self.smoothness
    
    def Cibler(self, cible=glm.vec3(0.0,0.0,0.0)):
        self.cible = self.position + self.orientation if cible == glm.vec3(0.0,0.0,0.0) else cible
        
    def getViewMatrice(self):
        return glm.lookAt(self.position, self.cible, self.directionHaut)
    
    def Update(self, input):        
        self.Deplacer(input.touchesClavier[sdl2.SDL_SCANCODE_W], input.touchesClavier[sdl2.SDL_SCANCODE_S],input.touchesClavier[sdl2.SDL_SCANCODE_D],input.touchesClavier[sdl2.SDL_SCANCODE_A])
        self.Orienter(input.sourisRelX, input.sourisRelY)

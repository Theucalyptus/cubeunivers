#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Aug  9 8:81:34 2019

@author: antoine
"""
import sdl2.ext #pour utiliser SDL (fénètre, contexte opengl et évènement (clavier/souris))
from OpenGL.GL import * # pour utiliser opengl
from OpenGL.GL.shaders import * #pour utiliser les shaders opengl
import glm #pour les calcules de matrices opengl


import os #pour acceder au fichier
import math #pour faire des calculs de distance (renderdistance)

#Import des classe code expres par moi
import Input #pour gerer les inputs clavier et souris donné par sdl
import Monde #pour le monde (joueurs, monstres, terrain)
import Data #pour ouvrir des fichiers de configuration


def Run() :


    config = Data.Fichier() #Chargement du fichier de la configuration

    renderDistance = 8 #Déclaration des variables avec leur valeur par défaut
    FOV = 70.0
    windowSizeX, windowSizeY = 800, 600
    cameraSmoothness = 20

    if os.path.isfile("config.json") : #Si le fichier de config existe, on charge les valeurs
        config.Charger('config.json')
        windowSizeX, windowSizeY = config.GetVariable('windowSizeX'), config.GetVariable('windowSizeY')
        renderDistance = config.GetVariable('renderDistance')
        FOV = config.GetVariable('FOV')
        cameraSmoothness = config.GetVariable('cameraSmoothness')
    else: config.path = "config.json" #Sinon on le crée juste

    #Création des eventuels répertoires manquants
    if os.path.isdir("Sauvegardes") == False:
        os.mkdir("Sauvegardes")
    if os.path.isdir("Sauvegardes/Mondes") == False:
        os.mkdir("Sauvegardes/Mondes")
    if os.path.isdir("Sauvegardes/Personnages") == False:
        os.mkdir("Sauvegardes/Personnages")

    #Initialisation de SDL2
    print("Initialisation de SDL2....")
    if sdl2.ext.init() == 0 : #Si sdl à pas démarrer
        print(sdl2.SDL_GetError())
        return -1 #on quite le programme
    print("Terminée") #sinon on contine

    #Création de la fenètre
    print("Création de la fenètre....")
    window = sdl2.SDL_CreateWindow(b"CubeUnivers", sdl2.SDL_WINDOWPOS_UNDEFINED, sdl2.SDL_WINDOWPOS_UNDEFINED, windowSizeX, windowSizeY, sdl2.SDL_WINDOW_OPENGL)
    if window == 0 : #si la fenètre ne c'est pas créer
         print(sdl2.SDL_GetError())
         return -1
    print("Terminée")

    #Définition des atributs du context OpenGL et création du context + paramètres SDL2
    sdl2.SDL_GL_SetAttribute (sdl2.SDL_GL_CONTEXT_MAJOR_VERSION, 3) #version de opengl à utiliser
    sdl2.SDL_GL_SetAttribute (sdl2.SDL_GL_CONTEXT_MINOR_VERSION, 3) #version de opengl à untiliser
    sdl2.SDL_GL_SetAttribute(sdl2.SDL_GL_CONTEXT_PROFILE_MASK, sdl2.SDL_GL_CONTEXT_PROFILE_CORE) #utilisation du profil optimale de opengl
    sdl2.SDL_GL_SetAttribute(sdl2.SDL_GL_DOUBLEBUFFER, 1) #paramètre sdl pour le rendu et la capture de la souris
    sdl2.SDL_GL_SetAttribute(sdl2.SDL_GL_DEPTH_SIZE, 24)
    sdl2.SDL_SetRelativeMouseMode(sdl2.SDL_ENABLE)
    sdl2.SDL_ShowCursor(sdl2.SDL_DISABLE)

    context = sdl2.SDL_GL_CreateContext(window) #création du contexte opengl (écran virtuel)
    glEnable(GL_DEPTH_TEST) #activation du module test de profondeur (pour pouvoir faire de la 3D :) )

    #Creation de la matrice projection
    far = math.sqrt((((renderDistance+5)*30/2)**2)*2)*1.1 #calcul de la distance de rendu
    projection = glm.perspective(float(FOV), float(windowSizeX) / float(windowSizeY), 0.1, far) #création de la matrice que converti notre monde 3d en image 2d

    #Création de la variable pour le monde et la gestion des périphériques (clavier et souris)
    monde = Monde.Monde("Monde 1", renderDistance, cameraSmoothness)
    _input = Input.Input()

    #Boucle principale (début du jeux)
    while _input.terminer != True:
        _input.UpdateEvents() #Mise à jour des evènement et input donner par SDL
        monde.Update(_input, sdl2.SDL_GetTicks()) #Mise à jour du monde (position du joueur, chunk...)

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)   #Vidage de l'écran
        monde.Rendre(projection)  #Rendu du monde
        sdl2.SDL_GL_SwapWindow(window) #Affichage du monde


    #Fermeture du programme
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
    sdl2.SDL_GL_SwapWindow(window)
    monde.Sauvegarder() #Sauvegarde du monde
    config.SetVariable('windowSizeX', windowSizeX)
    config.SetVariable('windowSizeY', windowSizeY)
    config.SetVariable('FOV', FOV)
    config.SetVariable('renderDistance', renderDistance)
    config.SetVariable('cameraSmoothness', cameraSmoothness)


    config.Sauvegarder() #Sauvegarde de la configuration
    sdl2.ext.quit() #Fermeture de SDL2



#Démarrage du programme
Run()

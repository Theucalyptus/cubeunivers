#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug 13 19:20:07 2019

@author: antoine
"""
from OpenGL.GL import *
from OpenGL.GL.shaders import *
import glm
import ctypes


class Shader:
    
    def __init__(self, nomShader):
        print("Compilation du shader " + nomShader + "....")
        with open('Shaders/' + nomShader + '.vert', 'r') as vertexsrc :
            with open('Shaders/' + nomShader + '.frag', 'r') as fragmentsrc :
                vertShader = compileShader(vertexsrc.read(), GL_VERTEX_SHADER)
                fragShader = compileShader(fragmentsrc.read(), GL_FRAGMENT_SHADER)
                self.program = compileProgram(vertShader, fragShader)
                print("Terminée")
                
    def Utiliser(self, value):
        if value == True :
            glUseProgram(self.program)
        else : glUseProgram(0)
    
    def UniformVec3(self, cible, vecteur):
        glUniform3fv(glGetUniformLocation(self.program, cible), 1, glm.value_ptr(vecteur))
        
    def UniformFloat(self, cible, variable):
        glUniform1fv(glGetUniformLocation(self.program, cible), 1, ctypes.c_float(variable))
    
    def UniformMat4(self, cible, matrice):
        glUniformMatrix4fv(glGetUniformLocation(self.program, cible), 1, GL_FALSE, glm.value_ptr(matrice))
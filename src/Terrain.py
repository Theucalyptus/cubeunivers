#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Aug  9 11:18:21 2019

@author: antoine
"""

import numpy
import math
import os

import Chunk
import Shader
import TerrainTools

class Terrain:

    def __init__(self, playerCoordX, playerCoordZ,seed, renderDistance, path):
        self.seed = seed
        self.renderDistance = renderDistance
        self.chunks = numpy.ndarray((self.renderDistance+1,self.renderDistance+1), dtype=numpy.object)
        self.playerCoordX, self.playerCoordZ, self.playerChunkCoordX, self.playerChunkCoordZ = playerCoordX, playerCoordZ, math.floor(playerCoordX/30),  math.floor(playerCoordZ/30)
        self.path = path

        self.shader = Shader.Shader('basic')

    def Charger(self) : #Charge le monde à partir des fichier
        for x in range(self.renderDistance+1):
            for z in range(self.renderDistance+1):
                self.chunks[x, z] = TerrainTools.ChargerChunk(x-self.renderDistance/2+self.playerChunkCoordX,z-self.renderDistance/2+self.playerChunkCoordZ, self.path)

    def Generer(self) :
        for x in range(self.renderDistance+1):
            for z in range(self.renderDistance+1):
                self.chunks[x,z] = TerrainTools.GenChunk(x-self.renderDistance/2+self.playerChunkCoordX,z-self.renderDistance/2+self.playerChunkCoordZ, self.seed)

    def Sauvegarder(self) :
        for x in range(self.renderDistance+1): #Sauvegarde les chunks
            for z in range(self.renderDistance+1):
                TerrainTools.SauvegarderChunk(self.chunks[x,z], self.path)



    def Update(self, playerCoordX, playerCoordZ) : #Met a jour le monde

        self.playerCoordX,self.playerCoordZ = playerCoordX, playerCoordZ

        playerChunkMvtX, playerChunkMvtZ = 0,0
        playerChunkMvtX = math.floor(playerCoordX/30) - self.playerChunkCoordX
        playerChunkMvtZ = math.floor(playerCoordZ/30) - self.playerChunkCoordZ
        self.chunks = numpy.roll(self.chunks, -playerChunkMvtZ, axis=1)
        self.chunks = numpy.roll(self.chunks, -playerChunkMvtX, axis=0)

        self.playerChunkCoordX += playerChunkMvtX
        self.playerChunkCoordZ += playerChunkMvtZ


        def RemplacerChunk(self,x,z):
            TerrainTools.SauvegarderChunk(self.chunks[x,z], self.path)

            #self.chunks[x,z] = Chunk.Chunk(x-self.renderDistance/2+self.playerChunkCoordX,z-self.renderDistance/2+self.playerChunkCoordZ, self.path + "x" + str(x-self.renderDistance/2+self.playerChunkCoordX) + "_z" + str(z-self.renderDistance/2+self.playerChunkCoordZ) + ".npz")

            if os.path.isfile(self.path + "x" + str(x-self.renderDistance/2+self.playerChunkCoordX) + "_z" + str(z-self.renderDistance/2+self.playerChunkCoordZ) + ".npz") == True:
                self.chunks[x,z] = TerrainTools.ChargerChunk(x-self.renderDistance/2+self.playerChunkCoordX, z-self.renderDistance/2+self.playerChunkCoordZ, self.path)
            else : self.chunks[x,z] = TerrainTools.GenChunk(x-self.renderDistance/2+self.playerChunkCoordX,z-self.renderDistance/2+self.playerChunkCoordZ, self.seed)


        if playerChunkMvtZ < 0 :
            for z in range(0-playerChunkMvtZ-1,-1,-1):
                for x in range(self.renderDistance+1):
                    RemplacerChunk(self,x,z)

        elif playerChunkMvtZ > 0:
            for z in range(self.renderDistance+1-playerChunkMvtZ,self.renderDistance+1):
                for x in range(self.renderDistance+1):
                    RemplacerChunk(self,x,z)

        if playerChunkMvtX < 0 :
            for x in range(0-playerChunkMvtX-1,-1,-1):
                for z in range(self.renderDistance+1):
                    RemplacerChunk(self,x,z)

        elif playerChunkMvtX > 0:
            for x in range(self.renderDistance+1-playerChunkMvtX,self.renderDistance+1, 1):
                for z in range(self.renderDistance+1):
                    RemplacerChunk(self,x,z)



    def Rendre(self, projection, view, sunLightPower, sunLightColor, sunLightVecteur, playerPosition, skyColor) : #Rend le monde
        for x in range(self.renderDistance+1):
            for z in range(self.renderDistance+1):
                self.chunks[x, z].Rendre(self.shader, projection, view, sunLightPower, sunLightColor, sunLightVecteur, playerPosition, skyColor)

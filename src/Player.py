#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Aug 12 10:51:40 2019

@author: antoine
"""
import os

import Data

class Player:
    
    data = Data.Fichier()
    
    def __init__(self, name):
        self.name = name
        self.path = "Sauvegardes/Personnages/" + name + "/"
        if os.path.isfile(self.path + "data.json"):
            self.Charger()
        else : self.Creer()
        
    def Creer(self):
        self.data.path = self.path + "data.json"
        
        
    def Charger(self):
        print("TODO")
        
    def Update(self):
        print("TODO")
        
        
    def Rendre(self):
        print("TODO")
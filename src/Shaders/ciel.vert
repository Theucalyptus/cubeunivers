#version 330 core

layout (location = 0) in vec3 in_Vertex;

uniform mat4 projection;
uniform mat4 view;
uniform mat4 model;

uniform vec3 couleur;
uniform float sunLightPuissance;


out vec4 VertColor;
out vec3 fragPos;
out vec3 ambLight;

void main()
{
	gl_Position = projection * view * model * vec4(in_Vertex, 1.0);
	
	VertColor = vec4(couleur, 1.0);
	fragPos = vec3(model * vec4(in_Vertex, 1.0));
	ambLight = float(max(sunLightPuissance-0.2, 0.1)) * couleur;
}

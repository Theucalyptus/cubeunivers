#version 330 core

layout (location = 0) in vec3 in_Vertex;
layout (location = 1) in vec4 in_Color;
layout (location = 2) in vec3 in_Normale;


uniform mat4 projection;
uniform mat4 model;
uniform mat4 view;

uniform float sunLightPuissance;
uniform vec3 sunLightColor;
uniform vec3 sunLightVecteur;

out vec4 couleurObjet;
out vec3 fragPos;
out vec3 ambLight;

void main()
{
	gl_Position = projection * view * model * vec4(in_Vertex, 1.0);
	
	vec3 vertPos = vec3(model * vec4(in_Vertex,1.0));
	vec3 sunLightDir = normalize(sunLightVecteur);
	vec3 norm = normalize(in_Normale);
	float sunLightImpact = max(dot(norm, sunLightDir), 0.0);
	vec3 lumiereSun = sunLightImpact * sunLightPuissance/2 * sunLightColor;
	ambLight = float(max(sunLightPuissance-0.2, 0.1)) * vec3(1.0,1.0,1.0);


	vec4 resultat = vec4(lumiereSun + ambLight, 1.0) * in_Color;		
	couleurObjet = resultat;

	fragPos = vertPos;

}

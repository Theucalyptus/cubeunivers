#version 330 core


in vec3 couleurObjet;

uniform vec3 couleur;

out vec4 couleurFinale;

void main()
{	
	couleurFinale = vec4(couleur, 1.0);
}

#version 330 core

in vec4 VertColor;
in vec3 fragPos;
in vec3 ambLight;

uniform vec3 playerPos;

out vec4 FragColor;

void main()
{
	float deltaX = fragPos.x-playerPos.x;
	float deltaZ = fragPos.z-playerPos.z;	
	float distance = sqrt(deltaX*deltaX+deltaZ*deltaZ);
	float coefFog = max(min((70-fragPos.y+distance/5)/(60.0+distance/10),1.0),0.0);
	vec4 couleurFog = (VertColor*0.8)*coefFog;
	vec4 couleurFrag = VertColor*(1-coefFog);
	FragColor = couleurFog*vec4(ambLight,1.0) + couleurFrag;
}

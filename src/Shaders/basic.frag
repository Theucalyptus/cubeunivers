#version 330 core

in vec4 couleurObjet;
in vec3 fragPos;
in vec3 ambLight;

uniform vec3 playerPos;
uniform vec3 skyColor;

out vec4 couleurFinale;

void main()
{
	float deltaX = fragPos.x-playerPos.x;
	float deltaZ = fragPos.z-playerPos.z;	
	float distance = sqrt(deltaX*deltaX + deltaZ*deltaZ);
	float coefFog = max(min((distance/200)-max(min(((fragPos.y-80)/30.0),1.0),0.0),0.95),0.0);	
	vec4 couleurFog = vec4(skyColor*0.8, 1.0)*coefFog;
	vec4 couleurFrag = couleurObjet*(1-coefFog);
	couleurFinale = couleurFog*vec4(ambLight,1.0) + couleurFrag;
}
